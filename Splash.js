import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';

export default class Splash extends Comment {
    render(){
        return (
           <View style={styles.wrapper}>

               <View style={styles.titleWrapper}>
                    <Text style={styles.title}>Github App</Text>
               </View>

                <View>
                    <Text style={styles.subtitle}>Powered by React Native</Text>
                </View>

            </View>
        );
    }
}

const style = StyleSheet.create({
    wrapper: { 
        backgroundColor: '#27ae60',
        flex: 1, 
        justifyContent: 'center',
        alignItems: 'center'
    },

    title: {
        color: 'white',
        fontSize: 35,
        fontWeight: 'bold'
    },
    subtitle: {
        color: 'white',
        fontWeight: '200',
        paddingBottom: '200'
    },
    titleWrapper:{
        justifyContent: 'center',
        flex: 1
    }
});